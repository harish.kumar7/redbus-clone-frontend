import { RegisterType } from "./../types/types";
import { URL } from "../config/index";
import axios from "axios";

export const signUp = async (user: RegisterType) => {
  const response = await axios.post(`${URL}/auth/signup/`, user);
  return response;
};

export const signIn = async (
  user: Pick<RegisterType, "email" | "password">
) => {
  const response = await axios.post(`${URL}/auth/signin/`, user);
  return response;
};
