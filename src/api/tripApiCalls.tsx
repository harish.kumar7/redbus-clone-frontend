import { URL } from "../config/index";
import axios from "axios";
import { TripSearchState } from "../types/types";

export const searchTripApi = async (data: Partial<TripSearchState>) => {
    const { from, to, date,isAc,isSleeper} = data
    let newDate;
    if (date) {
            newDate = new Date(date)     
            const link = `${URL}/trip/search/?from=${from}&to=${to}&onDate=${newDate.toISOString().slice(0, 10)}&requireAc=${isAc}&requireSleeper=${isSleeper}`
            const trips = await axios.get(link)
            return trips
    }
}

export const getSeatsApi = async (tripId: string) => {
    const seats = await axios.get(`${URL}/trip/seats/${tripId}`);
    return seats
}