import { getToken } from "./../helpers/authHelpers";
import { URL } from "../config/index";
import axios from "axios";
import { ITicketDataType } from "../types/types";

export const bookTicketsApi = async (
  ticketData: ITicketDataType,
  tripId: string | undefined
) => {
  const token = getToken();
  const res = await axios.post(`${URL}/booking/${tripId}`, ticketData, {
    headers: {
      Authorization: token ? token : "",
    },
  });
  return res.data;
};

export const getUserTickets = async () => {
  const token = getToken();
  const res = await axios.get(`${URL}/user/bookings/`, {
    headers: {
      Authorization: token ? token : "",
    },
  });
  return res.data;
};

export const deleteTicketApi = async (bookingId: string) => {
  const token = getToken();
  const res = await axios.delete(`${URL}/booking/${bookingId}/`, {
    headers: {
      Authorization: token ? token : "",
    },
  });
  return res;
};
