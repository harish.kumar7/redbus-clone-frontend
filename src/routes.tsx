import React from "react"
import {
    BrowserRouter as Router,
    Route,
    Routes,
} from "react-router-dom";
import Register from './features/register/Register';
import Login from './features/login/Login';
import SearchTrip from './features/searchtrip/SearchTrip';
import App from "./App"
import Seats from "./features/Seats/Seats"
import ProtectedRoutes from "./helpers/ProtectedRoutes"
import Booking from "./features/Bookings/Booking";
import PassangerDetails from "./features/PassangerDetails/passangerDetails";
const Routers = () => {
    return (
        <Router >
            <Routes>
                        <Route path="/"   element={<App />} />
                        <Route path="/login"  element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/seats" element={
                    <ProtectedRoutes>
                               <Seats />
                           </ProtectedRoutes>} />
                <Route path="/search" element={
                    <ProtectedRoutes>
                                <SearchTrip />
                     </ProtectedRoutes>
                } />
                <Route path="/bookings" element={
                    <ProtectedRoutes>
                                <Booking />
                     </ProtectedRoutes>
                } />
                  <Route path="/book/seats" element={
                    <ProtectedRoutes>
                            <PassangerDetails />
                     </ProtectedRoutes>
                } />
              
         </Routes>
        </Router> 
    )
}

export default Routers