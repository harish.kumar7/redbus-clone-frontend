import React from 'react'
import { Navigate} from 'react-router-dom'
import { isLoggedIn } from './authHelpers';

const ProtectedRoutes = ({children}:{ children: React.ReactNode}) => {
        if (!isLoggedIn()) {
            return  <Navigate to="/login" />
        } else {
            return <>{children}</>
        }
}

export default ProtectedRoutes;