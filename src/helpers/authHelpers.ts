export const saveToken = (data: string) => {
  if (typeof window !== "undefined") {
    localStorage.setItem("jwt", data);
  }
};

export const isLoggedIn = () => {
  if (localStorage.getItem("jwt")) {
    return true;
  } else {
    return false;
  }
};

export const signOut = () => {
  if (localStorage.getItem("jwt")) {
    localStorage.removeItem("jwt");
    return true;
  } else {
    return false;
  }
};

export const getToken = () => {
  return localStorage.getItem("jwt");
};
