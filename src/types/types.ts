/* 
Model types 
*/
export interface IUser {
  firstName: string;
  lastName?: string;
  email: string;
  password?: string;
  type: string;
  token?: string;
}

export interface PassangerData {
  passangerName: string[];
  passangerGender: string[];
  passangerAge: string[];
  error: string;
  success: boolean;
}

export interface IBooking {
  _id: string;
  tripId: string;
  userId: string;
  noOfBookedSeats: number;
  seatNo: string[];
  totalFair: number;
  passangerNames: string[];
  gender: string[];
  age: number[];
  bookedDate: Date;
  travellingDate: Date;
}

export interface ITicketDataType {
  noOfSeats: number;
  seatNo: string[];
  passangerNames: string[];
  gender: string[];
  age: number[];
}

export interface ISeat {
  seatNo: string;
  type: string;
  price: number;
  booked?: boolean;
  isSelected?: boolean;
}

export interface TripSearchState {
  from: string;
  to: string;
  date: Date;
  isAc: boolean;
  isSleeper: boolean;
  error?: string;
  success?: boolean;
  isLoading?: boolean;
  trips?: ITrip[];
}
export interface ITrip {
  _id: string;
  busId: string;
  operatorId: string;
  totalSeats: number;
  seat: ISeat[];
  availableSeats: string[];
  from: string;
  to: string;
  startDate: Date;
  endDate: Date;
  isAc: boolean;
  isSleeper: boolean;
  sleeperPrice: number;
  seaterPrice: number;
  upperPrice: number;
  totalSleeper: number;
  totalSeater: number;
  totalUpper: number;
  bookings: IBooking[];
  children?: React.ReactNode;
  message?: string;
}

export interface RegisterType {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  type: string;
}

export interface ResponseType {
  firstName: string;
  lastName?: string;
  email: string;
  type: string;
  token: string;
  error: string;
  success: boolean;
}
