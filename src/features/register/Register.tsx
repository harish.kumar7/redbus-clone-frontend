import React from 'react'
import { Box, Button,Container,createTheme,CssBaseline,Grid,MenuItem,Select,TextField,ThemeProvider,Typography} from "@mui/material";
import { RegisterType } from "../../types/types"
import { Link, Navigate } from "react-router-dom";
import { useAppDispatch,useAppSelector } from '../../app/hooks';
import Spinner from '../spinner/Spinner';
import { register, userAuth } from "../login/userSlice"
import Error from '../Error/Error';

const RegisterForm = () => {
    const [registerData, setRegisterData] = React.useState<RegisterType>({
        firstName: "cus",
        lastName: "cus",
        email: "cus122@gmail.com",
        password: "Harish@07",
        type:"customer"
    })
  
  const { firstName, lastName, email, password, type } = registerData
  
  const theme = createTheme();
  const dispatch = useAppDispatch()
  
  const { user, error, isLoading } = useAppSelector(userAuth)

    
    const handleChange = (name: string)=> (event: { target: { value: string; }})=>{
        setRegisterData({...registerData,[name]:event.target.value})
    }
    const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault()
        setRegisterData({
            ...registerData,
        })
        dispatch(register(registerData))
    }

  const SignUpForm = () => {
    return(
    <ThemeProvider theme={theme}>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              minWidth:120
            }}
          >
            <Typography component="h1" variant="h5">
              Sign up
            </Typography>
            <Box component="form" noValidate onSubmit={(e)=>onSubmit(e)} sx={{ mt: 3 }} >
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                      <TextField
                          name="firstName"
                          required
                          fullWidth
                          id="firstName"
                          label="First Name"
                          value={firstName}
                          onChange={handleChange("firstName")}
                        />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="lastName"
                    label="Last Name"
                    name="lastName"
                    autoComplete="family-name"
                    value={lastName}
                    onChange={handleChange("lastName")}       
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    value={email}
                    onChange={handleChange("email")}                    
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    value={password}  
                    onChange={handleChange("password")}                    
                    />
                  </Grid>
                      <Grid item xs={12}>
                            <Select
                              value={type}
                              label="type"
                              name="type"
                              sx={{ margin: '5px', minWidth: 431 }}
                              onChange={handleChange("type")}              
                            >
                              <MenuItem value={"operator"}     sx={{ margin: '5px', minWidth: 120 }} >operator</MenuItem>
                              <MenuItem value={"customer"}     sx={{ margin: '5px', minWidth: 120 }} >customer</MenuItem>
                           </Select>
                       </Grid>
              <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
              >
                Sign Up
              </Button>
              <Grid container justifyContent="flex-end">
                <Grid item>
                    <Link to="/login">Already have an account? Sign in</Link>
                </Grid>
              </Grid>
            </Grid>
          </Box>
        </Box>
    </Container>
    </ThemeProvider>
  )
  }
  
  return (
    <div>
      {isLoading ?  <Spinner /> :
        <>
            {
                error &&   <Error error={error} />
              }
          {
            SignUpForm()
          }
          {
             user &&  user.type==="customer"  && <Navigate to="/search" />
          }
        </>
      }
      </div>
    );
}


const  Register = () => <RegisterForm />

export default Register