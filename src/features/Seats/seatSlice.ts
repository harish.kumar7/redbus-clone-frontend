import { ISeat } from "./../../types/types";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getSeatsApi } from "../../api/tripApiCalls";
import { AxiosError } from "axios";
import { RootState } from "../../app/store";

type seat = {
  seatStatus: ISeat[];
  selectedSeats: string[];
  error: string;
  isLoading: boolean;
};

const initialState: seat = {
  seatStatus: [],
  selectedSeats: [],
  error: "",
  isLoading: false,
};

export const getSeats = createAsyncThunk(
  "trip/searchSeat",
  async (tripId: string, { rejectWithValue }) => {
    try {
      const data = await getSeatsApi(tripId);
      return data;
    } catch (err: unknown) {
      if (!(err instanceof AxiosError) || !err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data.errors);
    }
  }
);

export const seatSlice = createSlice({
  name: "seat",
  initialState,
  reducers: {
    addSelectedSeats: (state, action) => {
      action.payload.map((seat: string) => {
        state.selectedSeats.push(seat);
      });
    },
    removeSelectedSeats: (state) => {
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      state.selectedSeats = [];
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getSeats.pending, (state) => {
        return { ...initialState, isLoading: true };
      })
      .addCase(getSeats.fulfilled, (state, action) => {
        state.isLoading = false;
        state.seatStatus = action.payload?.data;
      })
      .addCase(getSeats.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload as string;
      });
  },
});

export const { addSelectedSeats, removeSelectedSeats, setError } =
  seatSlice.actions;

export const seat = (state: RootState) => state.seat;

export default seatSlice.reducer;
