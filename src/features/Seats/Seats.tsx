import AirlineSeatFlatOutlinedIcon from '@mui/icons-material/AirlineSeatFlatOutlined';
import { Button, Tooltip} from '@mui/material';
import React, { useEffect, useState } from 'react';
import {useLocation } from 'react-router-dom';
import { getSeats, seat } from './seatSlice'
import { ISeat } from '../../types/types';
import Navbar from '../Navbar/Navbar';
import Spinner from '../spinner/Spinner';
import "./Seats.css"
import { addSelectedSeats,setError } from './seatSlice';
import { useNavigate } from "react-router-dom"
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import Error from '../Error/Error';


const Seats: React.FC = () => {
    
    const location : any = useLocation();
    const tripId = location.state.tripId
    const [selectedSeats, setSelectedSeats] = useState<string[]>([]);
    const [totalFair, setTotalFair] = useState<number>(0);

    const dispatch = useAppDispatch()
    const navigate = useNavigate();

    const{seatStatus,error,isLoading} = useAppSelector(seat)
    

    const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, seat: ISeat) => {
        if (seat.booked) {
            dispatch(setError("Cannot book , the seat is booked already"))
        }
       else  if ((selectedSeats.includes(seat.seatNo))){
            setSelectedSeats((selectedSeats) => selectedSeats.filter((seatNo) => seatNo !== seat.seatNo));
             setTotalFair(prevFair => prevFair - seat.price)
         }
        else {
            setSelectedSeats(selectedSeats => [...selectedSeats, seat.seatNo])
            setTotalFair(prevFair => prevFair + seat.price)
        }
    }
    const handleBooking = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault()
        dispatch(addSelectedSeats(selectedSeats))
        navigate("/book/seats",{ state: { tripId: tripId } })
    }    

    useEffect(() => {
        if (tripId) dispatch(getSeats(tripId))
    },[tripId,dispatch])
    
    return (
        <>
            <Navbar />
            {
                isLoading ?  <Spinner /> :
                    <>
                        <div className="flex flex-wrap  md: justify-around">
                            <div>
                                {error &&  <Error error={error} />}
                                    <h3 style={{ margin: '15px' }}>Lower deck</h3>
                                    <div className="seat-container   w-72 md:w-96">
                                            {seatStatus.map((seat: ISeat) => {
                                                if (seat.type === "sleeper") {
                                                    return (
                                                        <Tooltip  title={`${seat.seatNo}`} key={seat.seatNo} >
                                                            <div style={{ display: 'inline-block' }} onClick={(e) => handleClick(e, seat)}  >
                                                                <AirlineSeatFlatOutlinedIcon fontSize='large' color={`${seat.booked ? "disabled" : selectedSeats.includes(seat.seatNo) ? "success" : "primary"}`} className='seat' />
                                                            </div>
                                                        </Tooltip>
                                                    )
                                                }
                                            })}
                                    </div>

                                    <h3 style={{ margin: '15px' }} >Upper deck</h3>
                                    <div className="seat-container   w-72 md:w-96">
                                        {seatStatus.map((seat: ISeat) => {
                                            if (seat.type === "upper") {
                                                return (
                                                    <Tooltip  title={`${seat.seatNo}`} key={seat.seatNo} >
                                                        <div style={{ display: 'inline-block' }} onClick={(e) => handleClick(e, seat)} key={seat.seatNo}>
                                                            <AirlineSeatFlatOutlinedIcon fontSize='large' color={`${seat.booked ? "disabled" : selectedSeats.includes(seat.seatNo) ? "success" : "primary"}`} className='seat' />
                                                            </div>
                                                        </Tooltip>
                                                )
                                            }
                                        })}
                                  </div>
                          </div>
                            <div className="  m-20 h-44">
                                    <h4 > Total tickets :  {selectedSeats.length}</h4>
                                    <h4 >Total fair : {Number(totalFair)}</h4>
                                    <Button variant="contained" color="error" className='text-center' onClick={(e) => handleBooking(e)} disabled={selectedSeats && selectedSeats.length < 0 }>Proceed</Button>
                            </div >     
                     </div>
                    </>
                    }     
            </>
    );
}

export default Seats