import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { AxiosError } from "axios";
import { bookTicketsApi } from "../../api/bookingApiCalls";
import { RootState } from "../../app/store";

type ticketDataType = {
  noOfSeats: number;
  seatNo: string[];
  passangerNames: string[];
  gender: string[];
  age: number[];
  message?: string;
  tripId?: string;
  isLoading?: boolean;
  error?: string;
};

const initialState: ticketDataType = {
  noOfSeats: 0,
  seatNo: [],
  passangerNames: [],
  gender: [],
  age: [],
};

export const bookTickets = createAsyncThunk(
  "passanger/bookTickets",
  async (ticketData: ticketDataType, { rejectWithValue }) => {
    try {
      const { tripId } = ticketData;
      delete ticketData.tripId;
      const data = await bookTicketsApi(ticketData, tripId);
      return data;
    } catch (err: unknown) {
      if (!(err instanceof AxiosError) || !err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data.errors);
    }
  }
);

export const passengerSlice = createSlice({
  name: "passanger",
  initialState,
  reducers: {
    setError: (state, action) => {
      state.error = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(bookTickets.pending, (state) => {
        state.isLoading = true;
        state.error = "";
      })
      .addCase(bookTickets.fulfilled, (state, action) => {
        state.isLoading = false;
        console.log(action.payload);
        state.message = action.payload as string;
      })
      .addCase(bookTickets.rejected, (state, action) => {
        state.isLoading = false;
        console.log(action);
        state.error = action.payload
          ? (action.payload as string)
          : "error in booking";
      });
  },
});

export const passanger = (state: RootState) => state.passanger;

export const { setError } = passengerSlice.actions;

export default passengerSlice.reducer;
