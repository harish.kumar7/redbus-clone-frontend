
import { Box, Button,Container,CssBaseline,MenuItem,Select,TextField} from "@mui/material";
import React from "react";
import { bookTickets, passanger} from './passangerDetailsSlice';
import { useLocation } from 'react-router-dom';
import { useNavigate } from "react-router-dom"
import { useAppDispatch,useAppSelector } from '../../app/hooks';
import Navbar from '../Navbar/Navbar';
import Spinner from "../spinner/Spinner";
import Error from '../Error/Error';


const PassangerDetails = () => {
  const selectedSeats = useAppSelector(state => state.seat.selectedSeats)
  const navigate = useNavigate();
  const dispatch = useAppDispatch()
  
  type passangerDataType = {
    [key:string] : string
  }

    const [passangerName, setPassangerName] = React.useState<passangerDataType>({});
    const [passangerGender, setPassangerGender] = React.useState<passangerDataType>({});
   const [passangerAge, setPassangerAge] = React.useState<passangerDataType>({});

    const location : any= useLocation();
    const tripId = location.state.tripId
    
  const {message , error , isLoading} = useAppSelector(passanger)

  const onSubmit = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
         e.preventDefault()
        const passangerNamesArray: string[] = []
        const  passangerGenderArray:string[] = []
        const  passangerAgeArray:number[] = []
        
        for (const name in passangerName) {
            passangerNamesArray.push(passangerName[name]); 
        }
        for (const gender in passangerGender) {
            passangerGenderArray.push(passangerGender[gender]); 
        }
        for (const age in passangerAge) {
            passangerAgeArray.push(+passangerAge[age]); 
        }

        const ticketData = {
            noOfSeats: selectedSeats.length,
            seatNo: selectedSeats,
            passangerNames:passangerNamesArray,
            gender:passangerGenderArray,
            age: passangerAgeArray,
            tripId : tripId
        }
    dispatch(bookTickets(ticketData))
  }

    const passangerDetailsForm = (seatNo : string) => {
        return (
            <Container component="main" maxWidth="xs" key={seatNo}>
            <CssBaseline />
            <div className="flex flex-col ">
              <h1>Details of passanger at { seatNo}</h1>
              <div className="my-1 w-48">  
              <TextField
                                  required
                                  fullWidth
                                  label="Name"
                                  name={seatNo}
                                  autoComplete="Name"
                                  value={passangerName[seatNo] ? passangerName[seatNo]  : ''}
                                  onChange={e => setPassangerName({ ...passangerName,[e.target.name] : e.target.value })}                    
                        />
              </div>
              <div className="my-1 w-68">
                    <Select
                                value={passangerGender[seatNo] ? passangerGender[seatNo]  : ''}
                                label="type"
                                name={seatNo}
                                sx={{minWidth: 190 }}
                                onChange={e => setPassangerGender({ ...passangerGender,[e.target.name] : e.target.value })}            
                      >                
                                  <MenuItem value={"male"}     sx={{ margin: '5px'}} >male</MenuItem>
                                  <MenuItem value={"female"}     sx={{ margin: '5px'}} >female</MenuItem>
                  </Select>
              </div>
              <div className="my-1 w-48">  
                        <TextField
                                    required
                                    fullWidth
                                    id="age"
                                    label="age"
                                    name={seatNo}
                                    autoComplete="age"
                                    value={passangerAge[seatNo] ? passangerAge[seatNo]  : ''}
                                    onChange={e => setPassangerAge({ ...passangerAge,[e.target.name] : e.target.value })}                 
                />
                </div>
              </div>

            </Container>
        )
    }
    
  return (
      <>
      <Navbar />
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
        {
        isLoading ? <Spinner />
        :
        <>
              {error && <Error error={error} />}
              
              {
                  selectedSeats.map((seatNo) => {
                      return <div key = {seatNo}>
                        {passangerDetailsForm(seatNo)}
                    </div>
                  })
              }
              {
                selectedSeats.length > 0 &&  <Button  variant="contained"  onClick={e=>onSubmit(e)}>  Book tickets  </Button>
              }
              {
                selectedSeats.length < 0 && <Error error={"Haven't selected any seats"} />
                
               }
          </>
        }
      </div>
       {
        message && !error && navigate("/bookings")
        }
    </>
  )
}

export default PassangerDetails