import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
import { Link, useNavigate } from 'react-router-dom';
import { isLoggedIn } from '../../helpers/authHelpers';
import { signOut } from "../../helpers/authHelpers"
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import {removeUser} from "../login/userSlice"
export default function Navbar() {
    
  const navigate = useNavigate()
  const dispatch = useAppDispatch()

  
  const handleClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    signOut()
    dispatch(removeUser())
    navigate("/login")
  }

    return (
      <>
        {
        !isLoggedIn()
          ?
          <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
              <Toolbar sx={{marginLeft: 'auto'}}>
                        <Link to="/login" >    <Button color="inherit" sx={{ color: 'white' }} > Login  </Button>  </Link> 
              </Toolbar>
            </AppBar>
            </Box>
                :
              <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static">
                <Toolbar sx={{ marginLeft: 'auto' }} >
                    <Link to="/search" >   <Button color="inherit" sx={{ color: 'white' ,padding:'0px 10px'}} > search </Button>  </Link>
                    <Link to="/bookings" >   <Button color="inherit" sx={{ color: 'white' ,padding:'0px 10px'}} > bookings </Button>  </Link>
                      <Link to="/login" >   <Button color="inherit" sx={{ color: 'white' ,padding:'0px 10px'}} onClick={(e)=>handleClick(e)}> Logout </Button>  </Link>
                  </Toolbar>
                </AppBar>
            </Box>
      }
    </>
  );
}
