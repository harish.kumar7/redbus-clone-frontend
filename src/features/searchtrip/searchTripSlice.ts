import { RootState } from "../../app/store";
import { AxiosError } from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { TripSearchState } from "./../../types/types";
import { searchTripApi } from "../../api/tripApiCalls";

const initialState: Pick<TripSearchState, "error" | "isLoading" | "trips"> = {
  error: "",
  isLoading: false,
  trips: [],
};

export const getTrips = createAsyncThunk(
  "trip/search",
  async (tripSearchDetails: TripSearchState, { rejectWithValue }) => {
    try {
      const data = await searchTripApi(tripSearchDetails);
      return data;
    } catch (err: unknown) {
      if (!(err instanceof AxiosError) || !err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data.error);
    }
  }
);

export const searchTripSlice = createSlice({
  name: "searchTrip",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getTrips.pending, (state) => {
        return { ...initialState, isLoading: true };
      })
      .addCase(getTrips.fulfilled, (state, action) => {
        state.isLoading = false;
        state.trips = action?.payload?.data.trips;
      })
      .addCase(getTrips.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload as string;
      });
  },
});

export const searchTrip = (state: RootState) => state.searchTrip;

export default searchTripSlice.reducer;
