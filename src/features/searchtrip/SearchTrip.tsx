import React from 'react'
import { Button, Grid, TextField, Checkbox } from '@mui/material'
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { DesktopDatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { TripSearchState } from '../../types/types';
import Trip from "../trip/trip"
import {ITrip} from "../../types/types"
import Spinner from '../spinner/Spinner';
import Navbar from '../Navbar/Navbar';
import { getTrips, searchTrip } from "./searchTripSlice"
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import Error from '../Error/Error';
import "./SearchTrip.css"

const SearchTrip = () => {
    const [tripSearchData, setTripSearchData] = React.useState<TripSearchState>({
        from: sessionStorage.getItem('from') as string,
        to: sessionStorage.getItem('to') as string,
        date: sessionStorage.getItem('date') as unknown as Date, 
        isAc: (sessionStorage.getItem('isAc') === "true"),
        isSleeper : (sessionStorage.getItem('isSleeper') === "true" ),
    })

    const dispatch = useAppDispatch()
    const { from, to, date, isAc, isSleeper } = tripSearchData
    const {trips,error,isLoading} = useAppSelector(searchTrip)
    
    const handleChange = (name: string) => (event: { target: { value: string }; }) => {      
        sessionStorage.setItem(name, event.target.value);
        setTripSearchData({ ...tripSearchData, [name]: event.target.value})
   }

    
    const handleDate = (e: any) => {
        setTripSearchData({ ...tripSearchData, 'date': e._d })   
        sessionStorage.date =  e._d
    }
    sessionStorage.setItem('isAc', isAc.toString())
    sessionStorage.setItem('isSleeper',isSleeper.toString())
 const handleSubmit = async  (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
     event.preventDefault()    
        setTripSearchData({
            ...tripSearchData,
           error: "",
        })
    dispatch(getTrips({from,to,date,isAc,isSleeper}))
    }

    
    return (
        <div>
        <Navbar />
        <div className='mainContainer' >
                <div className="flex items-center  flex-col  md:flex-row ">
                    <div className='flex  flex-col  justify-evenly  md:flex-row  '>
                        <div className=' my-3  lg:w-44 '>
                            <TextField
                                                name="from"
                                                required
                                                fullWidth
                                                id="from"
                                                label="from"
                                                inputProps={{ style: { color: "whitesmoke" } }}
                                                value={ from }
                                onChange={handleChange("from")}
                                    />
                        </div>
                        <div className=' my-3   lg:w-44'>
                                <TextField
                                                    name="to"
                                                    required
                                                    fullWidth
                                                    id="to"
                                                    label="to"
                                                    inputProps={{ style: { color: "whitesmoke" } }}
                                                    value={to}
                                                    onChange={handleChange("to")}
                                
                                        />
                            </div>
                            <div className=' my-3  lg:w-44'>
                                <LocalizationProvider dateAdapter={AdapterMoment}>
                                        <DesktopDatePicker
                                                        label="Date of travel"
                                                        InputProps={{ style: { color: "whitesmoke" } }}
                                                        value={date}
                                                        renderInput={(params) => <TextField {...params} />}
                                                        onChange={(e) => {
                                                        handleDate(e)
                                                    }
                                                    }    
                                        />
                                    </LocalizationProvider>
                        </div>
                    </div>
                    <div className='flex  mr-9 md:mr-0'>
                    <Checkbox
                            checked={isAc}
                            onChange={()=>setTripSearchData({ ...tripSearchData, error: '', success: false, isAc:!isAc})}
                            inputProps={{ style: { color: "whitesmoke" } }}
                        />
                        <p className='pt-2 text-slate-50' >AC</p>
                        </div>
                        <div className='flex'>
                            <Checkbox
                                    checked={isSleeper}
                                    onChange={()=>setTripSearchData({ ...tripSearchData, error: '', success: false, isSleeper:!isSleeper})}
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                                <p className='pt-2 pr-2 text-slate-50'>Sleeper</p>
                        </div>
                    <Button variant="contained" sx={{height:52}}  onClick={e=>handleSubmit(e)}>
                                search buses
                    </Button>
                    </div>
            </div>
            {
                isLoading ? <Spinner /> :
                    <div className="flex flex-col  md:flex-row">
                {
                    trips && trips?.map((trip:ITrip) =>{
                        return <Trip trip={trip} key={trip._id} />
                    })
                }{
                    error &&  <Error error={error} />
                 }
                </div>
            }
            
            </div>
  )
}

export default SearchTrip
