import React,{useEffect} from 'react'
import { IBooking } from '../../types/types'
import Spinner from '../spinner/Spinner';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import {Alert, Box,Button,CardActions,Typography} from '@mui/material';
import Navbar from '../Navbar/Navbar';
import { useAppDispatch , useAppSelector } from '../../app/hooks';
import { selectBookings, getBookings,deleteTicket } from './bookingSlice';
import Error from '../Error/Error';



const Booking = () => {
const dispatch = useAppDispatch()
    const { tickets, isLoading, error, message } = useAppSelector(selectBookings)
    
    const cancelTicket = (ticketId: string, e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault()
    dispatch(deleteTicket(ticketId))
}
    
useEffect(() => {
    dispatch(getBookings())
}, [message])
    
  return (
      <div>
          <Navbar />
          {
              message && <Alert severity="success">{message}</Alert>
          }
          {
              isLoading ? <Spinner /> :
                  <>
                      {
                          error ?   <Error error={error} /> :
                              tickets.map((booking: IBooking) => {
                                  const date = new Date(booking?.travellingDate)
                                  
                                  return (
                                    <div className = "inline-block " key={booking._id} >
                                          <Card  sx={{ minWidth: 275 }}  className = "flex flex-col grow justify-center w-24 pt-4 border-solid m-7 ">
                                          <CardContent>
                                              <Typography>Total seats : {booking.noOfBookedSeats}</Typography>
                                              <Typography>Total Fair : {booking.totalFair}</Typography>
                                              <Typography>Date : {date.toLocaleDateString()}</Typography>
                                              <Typography>Time: {date.toLocaleTimeString()}</Typography>
                                              <Typography>Seat number : {booking.seatNo}</Typography>
                                          </CardContent>
                                          <CardActions>
                                              <Button color="error" onClick={(e)=> cancelTicket(booking._id,e)}>Cancel</Button>
                                          </CardActions>
                                          </Card>
                                </div>
                              )
                          })
                  }
                  </>
          }
    </div>
  )
}

export default Booking


