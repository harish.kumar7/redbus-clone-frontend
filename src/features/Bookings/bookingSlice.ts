import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { AxiosError } from "axios";
import { RootState } from "../../app/store";
import { getUserTickets, deleteTicketApi } from "../../api/bookingApiCalls";
import { IBooking } from "../../types/types";

type BookingState = {
  tickets: IBooking[];
  error: string;
  isLoading: boolean;
  message?: string;
};

const initialState: BookingState = {
  tickets: [],
  isLoading: false,
  error: "",
};

export const getBookings = createAsyncThunk(
  "ticket/getAll",
  async (_, { rejectWithValue }) => {
    try {
      const data = await getUserTickets();
      return data;
    } catch (err: unknown) {
      if (!(err instanceof AxiosError) || !err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data.error);
    }
  }
);

export const deleteTicket = createAsyncThunk(
  "ticket/delete",
  async (bookingId: string, { rejectWithValue }) => {
    try {
      const data = await deleteTicketApi(bookingId);
      return data;
    } catch (err: unknown) {
      if (!(err instanceof AxiosError) || !err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data.error);
    }
  }
);
export const bookingsSlice = createSlice({
  name: "bookings",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getBookings.pending, (state) => {
        return { ...initialState, isLoading: true };
      })
      .addCase(getBookings.fulfilled, (state, action) => {
        state.isLoading = false;
        state.tickets = action.payload.bookings;
      })
      .addCase(getBookings.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload as string;
      })
      .addCase(deleteTicket.pending, (state) => {
        return { ...initialState, isLoading: true };
      })
      .addCase(deleteTicket.fulfilled, (state, action) => {
        state.isLoading = false;
        console.log(action.payload);
        state.message = action.payload.data.message;
      })
      .addCase(deleteTicket.rejected, (state, action) => {
        state.isLoading = false;
        console.log(action.payload);
        state.error = action.payload as string;
      });
  },
});

export const selectBookings = (state: RootState) => state.bookings;

export default bookingsSlice.reducer;
