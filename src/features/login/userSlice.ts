import { RegisterType } from "./../../types/types";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { IUser } from "../../types/types";
import { signIn, signUp } from "../../api/userApiCalls";
import { RootState } from "../../app/store";
import { saveToken } from "../../helpers/authHelpers";
import { AxiosError } from "axios";

type userStateType = {
  user: IUser | null;
  isLoading: boolean;
  error: string;
};

const initialState: userStateType = {
  user: null,
  isLoading: false,
  error: "",
};

type loginRequestType = Pick<RegisterType, "email" | "password">;

export const login = createAsyncThunk(
  "user/login",
  async (body: loginRequestType, { rejectWithValue }) => {
    try {
      const data = await signIn(body);
      return data;
    } catch (err: any) {
      if (!(err instanceof AxiosError) || !err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data.error);
    }
  }
);

export const register = createAsyncThunk(
  "user/register",
  async (body: RegisterType, { rejectWithValue }) => {
    try {
      const data = await signUp(body);
      return data;
    } catch (err: any) {
      if (!(err instanceof AxiosError) || !err.response) {
        throw err;
      }
      return rejectWithValue(err.response.data.error);
    }
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    addUser: (state, action) => {
      state.user = action.payload.user;
    },
    removeUser: (state) => {
      state.user = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state) => {
        return { ...initialState, isLoading: true };
      })
      .addCase(login.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload as string;
      })
      .addCase(login.fulfilled, (state, action) => {
        state.isLoading = false;
        state.user = action.payload.data;
        saveToken(action.payload.data.token);
      })
      .addCase(register.pending, (state) => {
        return { ...initialState, isLoading: true };
      })
      .addCase(register.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload as string;
      })
      .addCase(register.fulfilled, (state, action) => {
        state.isLoading = false;
        state.user = action.payload.data;
        saveToken(action.payload.data.token);
      });
  },
});

export const { addUser, removeUser } = userSlice.actions;

export const userAuth = (state: RootState) => state.user;

export default userSlice.reducer;
