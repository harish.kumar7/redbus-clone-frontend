import React from 'react'
import { Box, Button,Container,createTheme,CssBaseline,Grid,TextField,ThemeProvider,Typography} from "@mui/material";
import { RegisterType } from "../../types/types"
import{Navigate} from "react-router-dom"
import { Link } from "react-router-dom";
import { login, userAuth } from "./userSlice"
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import Spinner from '../spinner/Spinner';
import Error from '../Error/Error';

const Login = () => {
    const [loginData, setLoginData] = React.useState<Pick<RegisterType,"email"|"password">>({
      email: "cus1@gmail.com",
      password: "Harish@07",
    })
  const { email, password} = loginData

  const dispatch = useAppDispatch()

  const { user, error, isLoading } = useAppSelector(userAuth)
   
    const handleChange = (name: string)=> (event: { target: { value: string; }; })=>{
        setLoginData({...loginData,[name]:event.target.value})
    }
    const theme = createTheme();

    const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault()
        setLoginData({
            ...loginData
        })      
       dispatch( login({email,password}))
    }



      const LoginForm = () => {
        return (
          <ThemeProvider theme={theme}>
          <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              minWidth:120
            }}
          >
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <Box component="form" noValidate onSubmit={(e)=>onSubmit(e)} sx={{ mt: 3 }} >
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                          <TextField
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            value={email}
                            onChange={handleChange("email")}                    
                          />
                    </Grid>
                  <Grid item xs={12}>
                          <TextField
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            value={password}  
                            onChange={handleChange("password")}                    
                            />
                    </Grid>
                          <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                            >
                          Sign In
                          </Button>
                    <Grid container justifyContent="flex-end">
                      <Grid item>
                          <Link to="/register">Dont have an account ? Sign up</Link>
                      </Grid>
                      </Grid>
               </Grid>
              </Box>
            </Box>
            </ Container>
            </ThemeProvider >
  )
      }
      return (
        <div>
          {isLoading ?   <Spinner /> :
            <>
              {
                error &&   <Error error={error} />
              }
              {
                LoginForm()
              }
              {
               user&&  user.type==="customer" && <Navigate to="/search" />
              }
            
            </>
          }
          </div>
      );
}

export default Login