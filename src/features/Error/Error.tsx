import { Alert, Grid } from '@mui/material'
import React from 'react'

const Error = ({error}:{error:string}) => {
  return (
    <Grid container justifyContent = "center">
      <Alert severity="error" className=' flex items-center justify-center'>{error}</Alert>
      </Grid>
  )
}

export default Error