/* eslint-disable react/react-in-jsx-scope */
import "./trip.css"
import {ITrip} from "../../types/types"
import { MouseEvent } from "react"
import { useNavigate } from "react-router-dom"

const Trip = ({ trip }: { trip: ITrip }) => {
  
  const startDate = new Date(trip.startDate)
  const endDate = new Date(trip.endDate)
  const navigate = useNavigate();
  
  const handleClick = async (e: MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
    e.preventDefault();
    navigate("/seats", { state: { tripId: trip._id } })
  }
  
  return (
    <div className="container">
    <div className="card">
      <div className="box">
          <div className="content">
            <div className="date"> start date : <p>{startDate.toLocaleDateString()}</p></div>
            <div className="date" >start time : <p>{startDate.toLocaleTimeString()}</p></div>
            <div className="date" > end date : <p>{endDate.toLocaleDateString()}</p></div>
            <div className="date" >end time : <p>{endDate.toLocaleTimeString()}</p></div>
            <p>AvailableSeats : {trip.availableSeats.length}</p>
            <p>Seater Price : {trip.sleeperPrice}</p>
            <p>Sleeper Price : {trip.sleeperPrice}</p>
            <button onClick={(e)=>handleClick(e)}>Book tickets</button>
        </div>
      </div>
          </div>
        </div>
  )
}

export default Trip