import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../features/login/userSlice";
import seatReducer from "../features/Seats/seatSlice";
import passangerReducer from "../features/PassangerDetails/passangerDetailsSlice";
import bookingsReducer from "../features/Bookings/bookingSlice";
import searchTripReducer from "../features/searchtrip/searchTripSlice";
export const store = configureStore({
  reducer: {
    user: userReducer,
    seat: seatReducer,
    passanger: passangerReducer,
    bookings: bookingsReducer,
    searchTrip: searchTripReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
