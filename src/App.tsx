/* eslint-disable react/react-in-jsx-scope */
import { Link, Navigate } from "react-router-dom";
import "./App.css"
import { isLoggedIn } from "./helpers/authHelpers";
import Navbar from "./features/Navbar/Navbar"
function App() {
  return (
    <div className="App">
      {
        isLoggedIn() && <Navigate to="/search" />
      }
      <div>
      <Navbar />
        <h1 style={{display:"flex",alignItems:"center",justifyContent:"center"}}>welcome </h1>
        </div>
    </div>
  );
}

export default App;
